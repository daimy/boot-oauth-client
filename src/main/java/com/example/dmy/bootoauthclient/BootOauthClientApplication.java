package com.example.dmy.bootoauthclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

//开启资源服务
@EnableResourceServer
@SpringBootApplication
public class BootOauthClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(BootOauthClientApplication.class, args);
	}

}

