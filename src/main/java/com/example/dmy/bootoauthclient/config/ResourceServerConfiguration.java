package com.example.dmy.bootoauthclient.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

/**
 * @Author: daimengying
 * @Date: 2019/1/9 09:08
 * @Description:
 */
@Configuration
@EnableResourceServer
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {
    @Override
    public void configure(HttpSecurity http) throws Exception {
        //将web登录和oauth登录的manager共享，不然只能有一方生效
        http.antMatcher("/api/**").antMatcher("/user").authorizeRequests().anyRequest().authenticated();
    }
}
