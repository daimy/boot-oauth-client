package com.example.dmy.bootoauthclient.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: daimengying
 * @Date: 2019/1/7 15:26
 * @Description:access_token要从授权服务端获取
 * http://localhost:9001/user?access_token=d2916baa-d417-4daf-9319-8b8f01754f96
 */
@RestController
public class TestPointController {
    @GetMapping("/user")
    public String getUser() {
        return "hello";
    }
}
